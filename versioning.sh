#!/bin/env bash

set -eu

cd "${TARGET_REPO_ROOT}" || exit

# ---- <functions> ----
is_uint() { case $1 in '' | *[!0-9]*) return 1 ;; esac }
is_int() { case ${1#[-+]} in '' | *[!0-9]*) return 1 ;; esac }
is_unum() { case $1 in '' | . | *[!0-9.]* | *.*.*) return 1 ;; esac }
is_num() { case ${1#[-+]} in '' | . | *[!0-9.]* | *.*.*) return 1 ;; esac }
# ---- </functions> ----


if [[ -v CI ]]; then
    if [[ -v GITLAB_CI ]]; then
        if [[ -v CI_COMMIT_BRANCH ]]; then
            : "${GIT_BRANCH:=${CI_COMMIT_BRANCH}}"
        else
            # This is properly a tag commit, so just assume that this is the main branch
            : "${GIT_BRANCH:=main}"
        fi
    else
        echo "CI not supported"
        exit
    fi
else
    : "${GIT_BRANCH:=$(git rev-parse --abbrev-ref HEAD)}"
fi

: "${GIT_BRANCH_TYPE:=$(echo "${GIT_BRANCH}" | awk -F '/' '{ print $1 }')}"

: "${CURRENT_COMMIT:=$(git rev-parse HEAD)}"

: "${LAST_TAG:=$(git describe --abbrev=0)}"
if [ "${LAST_TAG:0:1}" = v ]; then
    : "${LAST_VERSION:=$(echo "${LAST_TAG}" | cut -c 2-)}" # Remove the `v` from the tag
else
    : "${LAST_VERSION:=$LAST_TAG}" 
fi

: "${VERSION_MAJOR:=$(echo "${LAST_VERSION}" | awk -F '.' '{ print $1 }')}"
: "${VERSION_MINOR:=$(echo "${LAST_VERSION}" | awk -F '.' '{ print $2 }')}"
: "${VERSION_PATCH:=$(echo "${LAST_VERSION}" | awk -F '.' '{ print $3 }')}"
: "${NEXT_VERSION:=${VERSION_MAJOR}.$((VERSION_MINOR + 1)).0}"

COUNT_SINCE_TAG=$(git rev-list --count "${LAST_TAG}".."${CURRENT_COMMIT}")
#LAST_COMMIT_HASH=$(git log -n 1 --pretty=format:"%H")

case "${GIT_BRANCH_TYPE}" in
main)
    if [ "${COUNT_SINCE_TAG}" -eq 0 ]; then
        # No tags inbetween -> so this is the new version
        : "${BACKEND_VERSION:=${LAST_VERSION}}"
        : "${BACKEND_VERSION_SHORTEN:=stable}"
    else
        : "${BACKEND_VERSION:=${NEXT_VERSION}-beta.${COUNT_SINCE_TAG}}"
        : "${BACKEND_VERSION_SHORTEN:=beta}"
    fi
    ;;
develop)
    : "${BACKEND_VERSION:=${NEXT_VERSION}-alpha.${COUNT_SINCE_TAG}}"
    : "${BACKEND_VERSION_SHORTEN:=alpha}"
    ;;
feature)
    : "${GIT_FEATURE_BRANCH_NAME:=$(echo "${GIT_BRANCH}" | awk -F '/' '{ print $2 }')}"
    : "${BACKEND_VERSION:=${NEXT_VERSION}-alpha.00.${COUNT_SINCE_TAG}.${GIT_FEATURE_BRANCH_NAME}}"
    ;;
**)
    echo "This branch is NOT compatible with Gitflow..."

    ISSUE_ID=$(echo "${GIT_BRANCH}" | awk -F '-' '{ print $1 }')

    if ! is_uint "${ISSUE_ID}"; then
        echo "Is not compatible with Gitlab branch numbering..."
        exit 1
    fi

    : "${BACKEND_VERSION:=${NEXT_VERSION}-alpha.00.${COUNT_SINCE_TAG}.${ISSUE_ID}}"
    ;;
esac
