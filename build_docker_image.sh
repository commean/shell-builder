#!/bin/bash

set -eu

for arg in "$@"; do
    case "${arg}" in
    --push | -p)
        PUSH=YES
        ;;
    --launch)
        LAUNCH=YES
        ;;
    esac
done

source $(dirname "$(realpath "$0")")/set_env.sh

if [ "${IMAGE_NAME}" = "<not set>" ]; then
    echo "Please set image name!"
    exit 1
fi

$OCI_BUILDER build \
    --build-arg VERSION="${VERSION}" \
    --tag "${IMAGE}" \
    "${TARGET_REPO_ROOT}" 

if [ "${PUSH}" = "YES" ]; then
#    $OCI_BUILDER login --username "${DOCKER_USERNAME}" --password "${DOCKER_PASSWORD}"
    $OCI_BUILDER push "${IMAGE}"

    if [[ -v BACKEND_VERSION_SHORTEN ]]; then
        $OCI_BUILDER tag "${IMAGE}" "${IMAGE_SHORTEN}"
        $OCI_BUILDER push "${IMAGE_SHORTEN}"
    fi
fi

# if [ "${LAUNCH}" = "YES" ]; then
#     RUN_PARAMETERS=""

#     if [ "${GUESS_SETTINGS}" = "YES" ]; then
#         RUN_PARAMETERS="-e "GIT_EMAIL=${GIT_EMAIL}" \
#                         -e "GIT_USERNAME=${GIT_USERNAME}" \
#                         -e "EDITOR=${EDITOR}""
#     fi

#     $OCI_BUILDER run --rm \
#         -it \
#         ${RUN_PARAMETERS} \
#         "${IMAGE}"
# fi
