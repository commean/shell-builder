#!/bin/env bash

set -eu

VARIABLES_BEFORE=$(compgen -v)

sourced=0
if [ -n "$BASH_VERSION" ]; then
  (return 0 2>/dev/null) && sourced=1 
else # All other shells: examine $0 for known shell binary filenames.
     # Detects `sh` and `dash`; add additional shell filenames as needed.
  case ${0##*/} in sh|-sh|dash|-dash) sourced=1;; esac
fi

if [ "$sourced" == "1" ]; then
    : "${THIS_SCRIPT:=${BASH_SOURCE[0]}}"
else
    : "${THIS_SCRIPT:=$(realpath "$0"))}"
fi
: "${PUSH:=NO}"
: "${LAUNCH:=NO}"
: "${GUESS_SETTINGS:=NO}"

: "${OCI_BUILDER:=docker}"

: "${BUILDER_REPO_ROOT:=$(dirname "${THIS_SCRIPT}")}"
: "${TARGET_REPO_ROOT:=$(dirname "${BUILDER_REPO_ROOT}")}"
: "${DOCKERFILE:=$(realpath "${TARGET_REPO_ROOT}/Dockerfile")}"

source "${BUILDER_REPO_ROOT}/versioning.sh"

: "${IMAGE_NAME:=<not set>}"
: "${VERSION:=${BACKEND_VERSION}}"
: "${IMAGE:="${IMAGE_NAME}:${VERSION}"}"
if [[ -v BACKEND_VERSION_SHORTEN ]]; then
    : "${VERSION_SHORTEN:=${BACKEND_VERSION_SHORTEN}}"
    : "${IMAGE_SHORTEN:="${IMAGE_NAME}:${VERSION_SHORTEN}"}"
fi
VARIABLES_AFTER=$(compgen -v)

NEW_VARIABLES=$(echo "${VARIABLES_BEFORE[@]}" "${VARIABLES_AFTER[@]}" | tr ' ' '\n' | sort | uniq -u | grep -v 'VARIABLES_BEFORE' | grep -v 'VARIABLES_AFTER')

for VARIABLE in ${NEW_VARIABLES[@]}; do
    echo "${VARIABLE}=${!VARIABLE}"
done
